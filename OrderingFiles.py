import os
import shutil
import time

folder_to_monitor = r"C:\Users\sesasa\Desktop"
image_folder = os.path.join(folder_to_monitor, "pdf_files")

# Ensure the image folder exists
if not os.path.exists(image_folder):
    os.makedirs(image_folder)

while True:
    for filename in os.listdir(folder_to_monitor):
        if filename.endswith(".pdf") or filename.endswith(".pdf"):
            source = os.path.join(folder_to_monitor, filename)
            destination = os.path.join(image_folder, filename)
            shutil.move(source, destination)
            print(f"Moved {filename} to Images folder.")
    time.sleep(10)  # Wait for 10 seconds before checking again
