from selenium import webdriver

# Set up the browser. You need to have the appropriate WebDriver installed for this to work.
driver = webdriver.Chrome()

# Navigate to the webpage
driver.get('https://en.wikipedia.org/wiki/Sam_Altman')

# Save the screenshot
driver.save_screenshot('screenshot_test.png')

# Close the browser
driver.quit()
