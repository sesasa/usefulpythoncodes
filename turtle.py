import turtle
import math
import colorsys

def draw_spiral(radius, color_intensity=1.0, screen_size=800):
    window = turtle.Screen()
    window.setup(screen_size, screen_size)
    window.bgcolor('black')
    
    spiral = turtle.Turtle()
    spiral.speed(0)  # Fastest drawing speed
    spiral.width(2)
    spiral.hideturtle()

    # Set the initial position
    spiral.penup()
    spiral.setpos(-radius, 0)
    spiral.pendown()

    for i in range(int(radius*2*math.pi)):
        # Set the color using the HSV color model 
        # and then convert it to the RGB model
        red, green, blue = colorsys.hsv_to_rgb(i/radius/2, color_intensity, color_intensity)
        spiral.pencolor(red, green, blue)
        
        # Move and turn the turtle
        spiral.forward(1)
        spiral.left(360/radius - 1)

    window.mainloop()

draw_spiral(100)
