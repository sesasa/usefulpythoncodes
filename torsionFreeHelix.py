import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

# Helix parameters
R = .2  # Radius of the helix
h = 0.1  # Height per turn
n_turns = 1  # Number of turns

# Circle parameters
circle_radius = 0.2  # Radius of the circle
n_points = 100  # Number of points on the helix

# Create the helix
t = np.linspace(0, 2 * np.pi * n_turns, n_points)
x_helix = R * np.cos(t)
y_helix = R * np.sin(t)
z_helix = h * t

# Function to plot the circle at a given position on the helix with different colored points
def plot_circle(ax, cx, cy, cz, radius):
    circle_points = 2  # Four points on the circle
    theta = np.linspace(0, 2 * np.pi, circle_points, endpoint=False)
    x_circle = cx + radius * np.cos(theta)
    y_circle = cy + radius * np.sin(theta)
    z_circle = cz + np.zeros_like(theta)

    # Colors for the points
    colors = ['red', 'green', 'blue', 'yellow']

    # Plot each point with a different color
    for i in range(circle_points):
        ax.scatter(x_circle[i], y_circle[i], z_circle[i], color=colors[i])

# Plotting
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')

# Plot the helix
ax.plot(x_helix, y_helix, z_helix, label='Helix', color='grey')

# Plot the circles along the helix with different colored points
for i in range(n_points):
    plot_circle(ax, x_helix[i], y_helix[i], z_helix[i], circle_radius)

# Set labels and show plot
ax.set_xlabel('X axis')
ax.set_ylabel('Y axis')
ax.set_zlabel('Z axis')
ax.legend()
plt.show()
