import math

def calculate_exit_velocity(diameter, volume, time):
    # Calculate the cross-sectional area of the needle
    A = math.pi * (diameter / 2)**2

    # Calculate the volume flow rate
    Q = volume / time

    # Calculate the exit velocity
    V = Q / A

    return V

# Given data
diameter = 0.16  # in mm
volume = 1000  # in mm^3 (1 mL = 1000 mm^3)
time = 5  # in seconds

exit_velocity = calculate_exit_velocity(diameter, volume, time)
print(f"The exit velocity of the fluid is {exit_velocity:.2f} mm/s.")

