import yfinance as yf
import matplotlib.pyplot as plt
import pandas as pd

# List of biotech stock symbols
stocks = [
    'IOVA',  # Iovance Biotherapeutics, Inc.
    'BEAM',  # Beam Therapeutics Inc.
    'CDNA',  # CareDx, Inc
    'NTRA',  # Natera, Inc.
    'CRSP',  # CRISPR Therapeutics
    'TWST',  # Twist Bioscience Corporation
    'FATE'   # Fate Therapeutics, Inc.
]

# Fetching data
data = yf.download(stocks, start='2016-01-01', end='2024-03-02')['Close']

# Normalizing data by each stock's maximum price
normalized_data = data.apply(lambda x: x / x.max())

# Fetching market caps and converting to millions with rounding
market_caps = {stock: round(yf.Ticker(stock).info['marketCap'] / 1e6) for stock in stocks}

# Plotting
plt.figure(figsize=(14, 8))
for stock in normalized_data.columns:
    plt.plot(normalized_data.index, normalized_data[stock], label=f'{stock} - Market Cap: {market_caps[stock]:,}M')

plt.title('Normalized Price History of Selected Biotech Stocks')
plt.xlabel('Date')
plt.ylabel('Normalized Price')
plt.legend()
plt.grid(True)

# Save the figure with high quality
save_path = 'Stocks_figs/BioTechStocks/normalized_biotech_stocks2016_20240302.png'
plt.savefig(save_path, dpi=300)  # Saving the figure with high DPI for high quality
print(f"Plot saved at {save_path}")


