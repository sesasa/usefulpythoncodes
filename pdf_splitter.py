import PyPDF2

def split_pdf(input_pdf, output_directory, initial_pages_to_merge):
    with open(input_pdf, 'rb') as file:
        reader = PyPDF2.PdfFileReader(file)

        # Check if PDF is encrypted
        if reader.isEncrypted:
            reader.decrypt("")

        # Merge the initial pages
        if initial_pages_to_merge > 0:
            writer = PyPDF2.PdfFileWriter()
            for page_num in range(initial_pages_to_merge):
                writer.addPage(reader.getPage(page_num))
            
            output_pdf = f"{output_directory}/merged_pages_1_to_{initial_pages_to_merge}.pdf"
            with open(output_pdf, 'wb') as output_file:
                writer.write(output_file)
            print(f"Saved: {output_pdf}")

        # Save the remaining pages as separate PDFs
        for page_num in range(initial_pages_to_merge, reader.numPages):
            writer = PyPDF2.PdfFileWriter()
            writer.addPage(reader.getPage(page_num))
            
            output_pdf = f"{output_directory}/{page_num + 1}.pdf"
            with open(output_pdf, 'wb') as output_file:
                writer.write(output_file)
            print(f"Saved: {output_pdf}")
            
input_file_path = r"C:\Users\sesasa\Desktop\3Dbeat\3Dbeat_meeting_Jan2024.pdf"
output_dir = r"C:\Users\sesasa\Desktop\3Dbeat\pdf_slides"

# Ask the user how many initial pages to merge
num_pages = int(input("How many initial pages do you want to merge? "))

split_pdf(input_file_path, output_dir, num_pages)
