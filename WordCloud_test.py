import matplotlib.pyplot as plt
from wordcloud import WordCloud

def generate_word_cloud(text):
    # Create a WordCloud object
    wordcloud = WordCloud(width = 800, height = 800, 
                          background_color ='white', 
                          stopwords = set(['the', 'of', 'and', 'to', 'in']), 
                          min_font_size = 10).generate(text)

    # Plot the WordCloud image                        
    plt.figure(figsize = (8, 8), facecolor = None) 
    plt.imshow(wordcloud) 
    plt.axis("off") 
    plt.tight_layout(pad = 0) 
    plt.show() 

sample_text = """
Python is an interpreted, high-level and general-purpose programming language. 
Python's design philosophy emphasizes code readability with its notable use of significant indentation. 
Its language constructs and object-oriented approach aim to help programmers write clear, logical code for small and large-scale projects.
Python is dynamically typed and garbage-collected. It supports multiple programming paradigms, 
including procedural, object-oriented, and functional programming.
"""

generate_word_cloud(sample_text)
