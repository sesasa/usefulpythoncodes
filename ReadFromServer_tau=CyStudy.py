import paramiko
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from io import StringIO
import os
import math

# Server details
hostname = 'sylg.fysik.dtu.dk'
username = 'sesasa'
password = os.getenv('Sylg_PASSWORD')

# File paths
file_paths = [
    '/home/mek/sesasa/Flagellates/3D/torsion_paraSweep/L40_N7/tau_study/alpha=_Cy/csv_plots/Ffl_tot__00050.csv',
    '/home/mek/sesasa/Flagellates/3D/torsion_paraSweep/L40_N7/tau_study/0pi/csv_plots/Ffl_tot__00050.csv',
    '/home/mek/sesasa/Flagellates/3D/torsion_paraSweep/L40_N7/tau_study/pi_6/csv_plots/Ffl_tot__00050.csv',
    '/home/mek/sesasa/Flagellates/3D/torsion_paraSweep/L40_N7/tau_study/pi_3/csv_plots/Ffl_tot__00050.csv',
    '/home/mek/sesasa/Flagellates/3D/torsion_paraSweep/L40_N7/N_study/0.5pi_N7/csv_plots/Ffl_tot__00050.csv',
    '/home/mek/sesasa/Flagellates/3D/torsion_paraSweep/L40_N7/tau_study/2pi_3/csv_plots/Ffl_tot__00050.csv',
    '/home/mek/sesasa/Flagellates/3D/torsion_paraSweep/L40_N7/tau_study/pi/csv_plots/Ffl_tot__00050.csv'
]

file_paths_p = [
    '/home/mek/sesasa/Flagellates/3D/torsion_paraSweep/L40_N7/tau_study/alpha=_Cy/csv_plots/Power__00050.csv',
    '/home/mek/sesasa/Flagellates/3D/torsion_paraSweep/L40_N7/tau_study/0pi/csv_plots/Power__00050.csv',
    '/home/mek/sesasa/Flagellates/3D/torsion_paraSweep/L40_N7/tau_study/pi_6/csv_plots/Power__00050.csv',
    '/home/mek/sesasa/Flagellates/3D/torsion_paraSweep/L40_N7/tau_study/pi_3/csv_plots/Power__00050.csv',
    '/home/mek/sesasa/Flagellates/3D/torsion_paraSweep/L40_N7/N_study/0.5pi_N7/csv_plots/Power__00050.csv',
    '/home/mek/sesasa/Flagellates/3D/torsion_paraSweep/L40_N7/tau_study/2pi_3/csv_plots/Power__00050.csv',
    '/home/mek/sesasa/Flagellates/3D/torsion_paraSweep/L40_N7/tau_study/pi/csv_plots/Power__00050.csv'
]

# Function to read a CSV file from a remote server
def read_csv_from_server(client, file_path):
    sftp_client = client.open_sftp()
    remote_file = sftp_client.open(file_path)
    file_content = remote_file.read().decode()
    remote_file.close()
    return pd.read_csv(StringIO(file_content))

# Create an SSH client instance
client = paramiko.SSHClient()
client.set_missing_host_key_policy(paramiko.AutoAddPolicy())

try:
    # Connect to the server
    client.connect(hostname, username=username, password=password)
    
    mean_values = []  # List to store mean of the third column for each DataFrame
    mean_values_p = []  # List to store mean of the second column for each DataFrame (power)

    # Loop over the file paths (forces) to read and process each file
    for i, file_path in enumerate(file_paths, start=1):
        df = read_csv_from_server(client, file_path)
        df.iloc[:, 2] *= 1e-12  # Scale the third column
        
        # Calculate mean of the third column and add it to the list
        mean_values.append(df.iloc[:, 2].mean())
        
        # Plotting scaled third column vs first column for each file
        plt.plot(df.iloc[1:, 0]*50, df.iloc[1:, 2], label=f'case{i-1}')
                    
        
    # Plotting the original data
    plt.xlabel('t/T')
    plt.ylabel('Fy (pN)')
    plt.title('Force: flagellum + hairs')
    plt.legend()
    plt.show()
    
    tau_val=[-0.45*math.pi, 0, 1/6*math.pi, 1/3*math.pi, 1/2*math.pi, 2/3*math.pi, 1*math.pi]
    
    # Plotting the mean of the scaled third column vs. [1, 2, 3, 4, 5, 6]
    plt.figure()  # Start a new figure for the second plot
    plt.plot(tau_val, mean_values, marker='o', linestyle='-', color='r')
    plt.xlabel('Read Number')
    plt.ylabel('Mean Fy (pN)')
    plt.title('Mean Force vs. Read Number')
    plt.grid(True)
    plt.show()
    
    
    # Loop over the file paths_p (power) to read and process each file
    for i, file_path in enumerate(file_paths_p, start=1):
        df = read_csv_from_server(client, file_path)
        df.iloc[:, 1] *= 1e-15  # Scale the third column
        
        # Calculate mean of the third column and add it to the list
        mean_values_p.append(df.iloc[:, 1].mean())
        
        # Plotting scaled third column vs first column for each file
        plt.plot(df.iloc[1:, 0]*50, df.iloc[1:, 1], label=f'case{i-1}')   
        
        
    # Plotting the original data
    plt.xlabel('t/T')
    plt.ylabel('Powe (fW)')
    plt.title('Total power')
    plt.legend()
    plt.show()
    
    # Plotting the mean of the scaled third column vs. [1, 2, 3, 4, 5, 6]
    plt.figure()  # Start a new figure for the second plot
    plt.plot(tau_val, mean_values_p, marker='o', linestyle='-', color='r')
    plt.xlabel('Read Number')
    plt.ylabel('Mean Power (fW)')
    plt.title('Mean power vs. Read Number')
    plt.grid(True)
    plt.show()        


    # Calculating F_norm
    F_norm = np.array(mean_values) / np.sqrt(np.array(mean_values_p))*np.sqrt(50)

    # Plotting F_norm
    plt.plot(tau_val,F_norm, marker='o', linestyle='-', color='b')
    plt.xlabel('Index')
    plt.ylabel('F_norm')
    plt.title('Normalized F values')
    plt.grid(True)
    plt.ylim(-10, 6)
    plt.show()



    # Now, create a DataFrame for writing to CSV
    first_column_values = tau_val
    df_to_write = pd.DataFrame({
        'tau': first_column_values,
        'F': mean_values,  # Using mean_values as per your note
        'P': mean_values_p
    })

    # Write the DataFrame to a CSV file
    df_to_write.to_csv('3D_csvData/Tau_cy_kappa_d_Effect.csv', index=False)
    
    
except Exception as e:
    print(f"An error occurred: {e}")
finally:
    # Close the connection
    client.close()
