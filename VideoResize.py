import subprocess
import os

def reduce_file_size(input_file_path, output_file_path, target_size_mb):
    # Get the size of the input file in bytes
    input_size = os.path.getsize(input_file_path)

    # Convert the target size to bytes
    target_size = target_size_mb * 1024 * 1024

    # Calculate the bitrate ratio (target size / current size)
    ratio = target_size / input_size

    # Use FFmpeg to reduce the file size
    cmd = f'ffmpeg -i "{input_file_path}" -b:v {int(ratio*100)}k "{output_file_path}"'
    subprocess.run(cmd, shell=True)

# Path of the input video
input_file_path = r"C:\Users\sesasa\Desktop\PythonCodes\videos\1.MOV"
# Path of the output video
output_file_path = r"C:\Users\sesasa\Desktop\PythonCodes\videos\1_reduced.MOV"
# Target file size in MB
target_size_mb = 50  # Adjust this based on your requirement

reduce_file_size(input_file_path, output_file_path, target_size_mb)
