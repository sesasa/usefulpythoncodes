import yfinance as yf
import matplotlib.pyplot as plt

# Fetch historical data
glxy = yf.download('GLXY.TO', start='2020-01-01', end='2024-02-20')  # Assuming GLXY.TO for Galaxy Digital Holdings on TSX
btc = yf.download('BTC-USD', start='2020-01-01', end='2024-02-20')

# Normalize the prices by their respective maximum price
glxy_normalized = glxy['Close'] / glxy['Close'].max()
btc_normalized = btc['Close'] / btc['Close'].max()

# Plotting
plt.figure(figsize=(14, 7))
plt.plot(glxy_normalized, label='GLXY (Normalized)', color='blue')
plt.plot(btc_normalized, label='Bitcoin (Normalized)', color='orange')

plt.title('GLXY vs. Bitcoin Normalized Prices (2020-2023)')
plt.xlabel('Date')
plt.ylabel('Normalized Price')
plt.legend()
plt.grid(True)
plt.show()
