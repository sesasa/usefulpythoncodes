
import yfinance as yf
import matplotlib.pyplot as plt
import pandas as pd

# Define the tickers
tickers = {
   # "S&P 500": "^GSPC",
    "Microsoft": "MSFT",
    "Amazon": "AMZN",
    #"Bitcoin": "BTC-USD",
  #  "Dogecoin": "DOGE-USD"
}

# Download historical data
start_date = "2020-11-30"
end_date = "2024-01-12"
data = {ticker: yf.download(tickers[ticker], start=start_date, end=end_date)['Close'] for ticker in tickers}

# Create a DataFrame
df = pd.DataFrame(data)

# Plotting
plt.figure(figsize=(15, 7))
for ticker in tickers:
    plt.plot(df[ticker], label=ticker)

plt.title("Historical Prices: S&P 500, Microsoft, Amazon, Bitcoin, Dogecoin")
plt.xlabel("Date")
plt.ylabel("Price")
plt.legend()
plt.show()
