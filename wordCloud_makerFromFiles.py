import PyPDF2
from wordcloud import WordCloud
import matplotlib.pyplot as plt

# Function to extract text from a PDF
def extract_text_from_pdf(pdf_path):
    with open(pdf_path, 'rb') as file:
        reader = PyPDF2.PdfFileReader(file)
        text = ''
        for page_num in range(reader.numPages):
            page = reader.getPage(page_num)
            text += page.extractText()
    return text

pdf_path = r"C:\Users\sesasa\Desktop\pdf_files\FlagellatesSA.pdf"
text = extract_text_from_pdf(pdf_path)

# Create a WordCloud object
wordcloud = WordCloud(width=480, height=480, margin=0).generate(text)

# Display the generated image using matplotlib
plt.imshow(wordcloud, interpolation='bilinear')
plt.axis("off")
plt.margins(x=0, y=0)
plt.show()
