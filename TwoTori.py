import numpy as np
import matplotlib.pyplot as plt

def plot_torus(ax, r, R, color='b'):
    """Plot a torus on the given Axes3D object."""
    u = np.linspace(0, 2 * np.pi, 100)
    v = np.linspace(0, 2 * np.pi, 100)
    U, V = np.meshgrid(u, v)
    X = (R + r * np.cos(V)) * np.cos(U)
    Y = (R + r * np.cos(V)) * np.sin(U)
    Z = r * np.sin(V)
    
    ax.plot_surface(X, Y, Z, color=color, alpha=0.6)  # Setting alpha to 0.5 makes it transparent

# Define the radii for the smaller torus
r_small = 1
R_small = 5

# Calculate the scale factor for the larger torus
scale_factor = 20**(1/2)
r_large = r_small * scale_factor
R_large = R_small * 1.9

# Create a 3D plot
fig = plt.figure(figsize=(10, 5))
ax = fig.add_subplot(111, projection='3d')

# Plot the smaller torus
plot_torus(ax, r_small, R_small, color='blue')

# Plot the larger torus
plot_torus(ax, r_large, R_large, color='red')

# Adjust the view angle for better visualization
ax.view_init(elev=30, azim=50)

plt.show()
