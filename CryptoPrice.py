import yfinance as yf
import pandas as pd
import matplotlib.pyplot as plt

# List of cryptocurrency symbols, now including Solana (SOL-USD)
#cryptos = ['BTC-USD', 'DOGE-USD', 'POND-USD', 'ETH-USD', 'ATOM-USD', 'ETC-USD', 'SOL-USD']
cryptos = ['BTC-USD', 'DOGE-USD', 'POND-USD', 'ETH-USD', 'ATOM-USD', 'ETC-USD', 'SOL-']
# Dictionary to hold the normalized data
normalized_data = {}

# Fetch price history and normalize
for crypto in cryptos:
    # Fetch historical data
    data = yf.download(crypto, start="2021-01-01", end="2023-03-03")
    
    # Assuming 'Close' price is of interest
    close_prices = data['Close']
    
    # Find the maximum price
    max_price = close_prices.max()
    
    # Normalize the prices
    normalized_prices = close_prices / max_price
    
    # Store in the dictionary
    normalized_data[crypto] = normalized_prices

# Convert the dictionary to a DataFrame for better visualization
normalized_df = pd.DataFrame(normalized_data)

# Plotting
plt.figure(figsize=(14, 8))
for crypto in normalized_df.columns:
    plt.plot(normalized_df.index, normalized_df[crypto], label=crypto)

plt.title('Normalized Price Comparison of Cryptocurrencies Including Solana')
plt.xlabel('Date')
plt.ylabel('Normalized Price')
plt.legend()
plt.grid(True)

# Specify the directory and filename for the plot
output_directory = "Stocks_figs/crypto_plots"  # Adjust the path as needed
filename = "crypto_price_comparison230302.png"

# Full path
full_path = f"{output_directory}/{filename}"

# Saving the plot with high quality (e.g., 300 DPI)
plt.savefig(full_path, dpi=300)

# Notify the user
print(f"Plot saved successfully at {full_path}")
