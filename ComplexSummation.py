import numpy as np
import matplotlib.pyplot as plt

# Define the range of theta from 0 to 100
theta_values = np.linspace(0, 10000, 10000)

# Initialize arrays for storing the summation points
sum_x = []
sum_y = []

# Loop over theta values
for theta in theta_values:
    # Convert theta to radians
    rad = np.radians(theta)

    # Calculate e^(i*theta) and e^(i*pi*theta)
    e_i_theta = np.exp(1j * rad)
    e_i_pi_theta = np.exp(1j * np.pi * rad)

    # Calculate the summation
    summation = e_i_theta + e_i_pi_theta

    # Store the real and imaginary parts
    sum_x.append(summation.real)
    sum_y.append(summation.imag)

# Plot the summation points
plt.figure(figsize=(8, 8))
plt.plot(sum_x, sum_y, 'r-')
plt.xlim(-2, 2)
plt.ylim(-2, 2)
plt.title("Plot of e^(i*theta) + e^(i*pi*theta) for theta = 0 to 100")
plt.xlabel("Real Part")
plt.ylabel("Imaginary Part")
plt.grid(True)
plt.show()
