import yfinance as yf
import matplotlib.pyplot as plt
import pandas as pd

# List of chip stock symbols
stocks = [
    'NVDA',  # Nvidia Corporation
    'AMD',   # Advanced Micro Devices, Inc.
    'AVGO',  # Broadcom Inc. (BroadCom's ticker is AVGO)
    'ASML',  # ASML Holding NV
    'AMAT',  # Applied Materials, Inc
    'QCOM'   # Qualcomm Incorporated
]

# Fetching data
data = yf.download(stocks, start='2016-06-01', end='2024-03-02')['Close']

# Normalizing data by each stock's maximum price
normalized_data = data.apply(lambda x: x / x.max())

# Fetching market caps, converting to millions, and rounding without decimals
market_caps = {stock: round(yf.Ticker(stock).info['marketCap'] / 1e6) for stock in stocks if 'marketCap' in yf.Ticker(stock).info}

# Plotting
plt.figure(figsize=(14, 8))
for stock in normalized_data.columns:
    plt.plot(normalized_data.index, normalized_data[stock], label=f'{stock} - Market Cap: {market_caps[stock]}M')

plt.title('Normalized Price History of Selected Chip Stocks')
plt.xlabel('Date')
plt.ylabel('Normalized Price')
plt.legend()
plt.grid(True)

# Save the figure with high quality
save_path = 'Stocks_figs/Chip_Stocks/normalized_chip_stocks_prices2016_240302.png'
plt.savefig(save_path, dpi=300)  # Saving the figure with high DPI for high quality
print(f"Plot saved at {save_path}")
