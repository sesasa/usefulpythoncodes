import os
import PyPDF2

# Ensure that 'pdf_files' subdirectory exists (test for simin)
if not os.path.exists('pdf_files'):
    print("The 'pdf_files' directory does not exist.")
    exit()

def get_pdf_names():
    # Ask the user for the names of the PDF files to merge
    pdf_names = input("Enter the names of the PDF files to merge, separated by a space: ").split()
    # Check if each provided file name exists
    for pdf_name in pdf_names:
        if not os.path.exists(os.path.join('pdf_files', pdf_name + '.pdf')):
            print(f"File '{pdf_name}' does not exist in 'pdf_files'.")
            exit()
    return pdf_names

def merge_pdfs(pdf_names):
    # Create a PDF merger object
    pdf_merger = PyPDF2.PdfMerger()
    
    for pdf_name in pdf_names:
        # Open each PDF file in binary read mode
        with open(os.path.join('pdf_files', pdf_name + '.pdf'), 'rb') as file:
            # Append the PDF file to the merger object
            pdf_merger.append(file)
    
    # Define the name of the merged PDF file
    merged_pdf_name = "merged.pdf"
    # Create/overwrite the merged PDF
    with open(merged_pdf_name, 'wb') as output_file:
        pdf_merger.write(output_file)
    
    print(f"The PDFs have been merged into '{merged_pdf_name}'.")

def main():
    pdf_names = get_pdf_names()
    merge_pdfs(pdf_names)

if __name__ == "__main__":
    main()
