import yfinance as yf
import matplotlib.pyplot as plt
import matplotlib.dates as mdates

# Define the stock ticker symbol
ticker_symbol = 'SMCI'

# Download the stock data
data = yf.download(ticker_symbol, start='2023-06-01', end='2024-03-20')

# Calculating rolling mean (smoothing) for the Volume and Close price
window_size = 1  # Window size for the rolling mean
smoothed_volume = data['Volume'].rolling(window=window_size).mean()
smoothed_close_price = data['Close'].rolling(window=window_size).mean()

# Creating a figure and a set of subplots
fig, ax1 = plt.subplots()

# Plotting the smoothed close price
color = 'tab:red'
ax1.set_xlabel('Date')
ax1.set_ylabel('Smoothed Close Price', color=color)
ax1.plot(smoothed_close_price.index, smoothed_close_price, color=color)
ax1.tick_params(axis='y', labelcolor=color)

# Format the x-axis to display only year and month
ax1.xaxis.set_major_locator(mdates.MonthLocator())
ax1.xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m'))

#Creating a second y-axis for the smoothed volume plot
ax2 = ax1.twinx()
color = 'tab:blue'
ax2.set_ylabel('Smoothed Volume', color=color)  # Adjusted label for clarity
ax2.plot(smoothed_volume.index, smoothed_volume, color=color)
ax2.tick_params(axis='y', labelcolor=color)

# Title and layout adjustments
plt.title('Super Micro Computer, Inc. (SMCI) Stock Prices and Volume (Smoothed)')
fig.autofmt_xdate()  # Auto formats the x-axis dates for better readability
fig.tight_layout()

# Show the plot
plt.show()
