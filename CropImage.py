import os
from PIL import Image

def crop_margins_from_images(directory, x1, x2, y1, y2):
    """
    Crops margins from all JPEG images in the specified directory.
    
    Parameters:
    - directory: Path to the directory containing JPEG images.
    - x1: Percentage of the left margin to cut.
    - x2: Percentage of the right margin to cut.
    - y1: Percentage of the top margin to cut.
    - y2: Percentage of the bottom margin to cut.
    """
    for filename in os.listdir(directory):
        if filename.lower().endswith(".png"):
            img_path = os.path.join(directory, filename)
            with Image.open(img_path) as img:
                width, height = img.size
                
                # Calculate the cropping coordinates
                left = width * (x1 / 100.0)
                right = width - (width * (x2 / 100.0))
                top = height * (y1 / 100.0)
                bottom = height - (height * (y2 / 100.0))
                
                # Crop the image
                img_cropped = img.crop((left, top, right, bottom))
                
                # Save the cropped image, optionally you can overwrite or save as a new file
                cropped_img_path = os.path.join(directory, f"c_{filename}")
                img_cropped.save(cropped_img_path)

# Example usage
directory = r"C:\Users\sesasa\Desktop\3D_codes\sim_file\PNGs\mean"
crop_margins_from_images(directory, x1=5, x2=0, y1=0, y2=12)
