import matplotlib.pyplot as plt
import numpy as np
from matplotlib import patches

# Function to draw a single microtubule doublet
def draw_microtubule_doublet(ax, center, radius=0.3, separation=0.4, angle=0, length=0.5):
    d_angle = np.deg2rad(angle)
    x1, y1 = center
    x2 = x1 + np.cos(d_angle) * separation
    y2 = y1 + np.sin(d_angle) * separation
    circle1 = plt.Circle((x1, y1), radius, color='blue', fill=False, linewidth=8)
    circle2 = plt.Circle((x2, y2), radius, color='blue', fill=False, linewidth=8)
    ax.add_artist(circle1)
    ax.add_artist(circle2)

# Function to draw the central pair of microtubules
def draw_central_pair(ax, center, radius=0.3):
    x, y = center
    circle1 = plt.Circle((x - radius-0.15, y+0.12), radius, color='green', fill=False, linewidth=8)  # You can adjust the linewidth value as needed
    circle2 = plt.Circle((x + radius+0.15, y-0.12), radius, color='green', fill=False, linewidth=8)
    ax.add_artist(circle1)
    ax.add_artist(circle2)

# Function to draw the membrane circle around the microtubules
def draw_membrane(ax, center, radius):
    membrane1 = plt.Circle(center, radius-0.2, color='blue', fill=False, linewidth=2)
    membrane2 = plt.Circle(center, radius, color='blue', fill=False, linewidth=2)
    ax.add_artist(membrane1)
    ax.add_artist(membrane2)

# Function to draw the coordinate system (T, N, B) with labels near the tips of the vectors
def draw_coordinate_system(ax, center, scale=1):
    n_tip = (-center[0], -center[1] - scale)
    ax.annotate('', xy=n_tip, xytext=center, arrowprops=dict(facecolor='black', shrink=0, width=2, headwidth=18))
    ax.text(*n_tip, '', color='black', fontsize=26, ha='center', va='bottom')
    b_tip = (-center[0] + scale, -center[1])
    ax.annotate('', xy=b_tip, xytext=center, arrowprops=dict(facecolor='black', shrink=0, width=2, headwidth=18))
    ax.text(center[0] - scale, center[1]-0.6, '', color='black', fontsize=26, ha='left', va='center')
    ax.plot(center[0], center[1], 'ko')  # Black dot for T axis
    ax.text(center[0] + 0.2, center[1] - 0.3, '', color='black', fontsize=26, ha='right', va='top')

# Function to draw a dashed line with an arbitrary angle \psi(s,t) with respect to N vector
def draw_dashed_line_with_angle(ax, center, scale=1, psi_angle_deg=30):
    psi_angle_rad = np.radians(psi_angle_deg)
    end_point_x = center[0] - scale * np.cos(psi_angle_rad)
    end_point_y = center[1] + scale * np.sin(psi_angle_rad)
    ax.plot([center[0] + 4, end_point_x], [center[1]-2.3, end_point_y], 'k:', linewidth=3)
    arc = patches.Arc(center, 3.0, 3.5, angle=-30, theta1=0, theta2=psi_angle_deg, color='black', linestyle='solid', linewidth=3)
    ax.add_patch(arc)
    #label_x = center[0] + 0.3 * np.cos(psi_angle_rad / 2)
    #label_y = center[1] + 0.3 * np.sin(psi_angle_rad / 2)
    #ax.text(label_x - 2.8, label_y + 0.5, r'$\psi(s,t)$', fontsize=22, color='black')


# Redefining the essential functions for context
def draw_line(ax, position, angle_deg, length, color='black', thickness=1):
    """Draw a line with specified position, angle, and length."""
    angle_rad = np.radians(angle_deg)
    end_x = position[0] + length * np.cos(angle_rad)
    end_y = position[1] + length * np.sin(angle_rad)
    ax.plot([position[0], end_x], [position[1], end_y], color=color, linewidth=thickness)


# Initialize figure and axis
fig, ax = plt.subplots(figsize=(10, 10))
ax.set_aspect('equal')
ax.axis('off')

# Draw the microtubule structure, membrane, and coordinate system
doublet_radius = 0.3
separation = 0.55
outer_radius = 3 + doublet_radius + separation
for i in range(9):
    angle = i * (360 / 9) -5
    x = 3 * np.cos(np.deg2rad(angle))
    y = 3 * np.sin(np.deg2rad(angle))
    draw_microtubule_doublet(ax, (x, y), radius=doublet_radius, separation=separation, angle=angle + 90)

draw_central_pair(ax, (0, 0))
draw_membrane(ax, (0, 0), outer_radius)
draw_coordinate_system(ax, (0, 0), scale=2.6)

# Draw the dashed line with angle \psi

draw_dashed_line_with_angle(ax, (0, 0), scale=4.4, psi_angle_deg=30)



# Drawing a line with the newly defined function
draw_line(ax, position=(3.1, -1.8), angle_deg=-30, length=5, color='red', thickness=14)
draw_line(ax, position=(-3.1, 1.8), angle_deg=150, length=5, color='red', thickness=14)

plt.xlim(-outer_radius-4, outer_radius+4)
plt.ylim(-outer_radius-0.0, outer_radius+0.0)
plt.show()
