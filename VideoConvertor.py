from moviepy.video.io.ffmpeg_tools import ffmpeg_extract_subclip
from moviepy.editor import VideoFileClip

def mp4_to_gif(input_file, output_file, start_time, end_time):
    try:
        # Extract the desired subclip from the MP4 file
        ffmpeg_extract_subclip(input_file, start_time, end_time, targetname="temp.mp4")

        # Load the subclip and convert it to a GIF
        video_clip = VideoFileClip("temp.mp4")
        video_clip.write_gif(output_file, fps=20)

        # Clean up the temporary video file
        video_clip.close()
        os.remove("temp.mp4")

        print(f"Conversion complete: {output_file}")
    except Exception as e:
        print(f"Error: {str(e)}")

if __name__ == "__main__":
    input_file = "C:/Users/sesasa/Documents\MyProjectVisuzalizations" \
    "\Projects\SpongePump\FlowOfparticlesTopView.mp4"  # Replace with your input MP4 file
    output_file = "C:/Users/sesasa/Documents\MyProjectVisuzalizations" \
    "\Projects\SpongePump\FlowOfparticlesTopView.gif"  # Replace with the desired output GIF file

    start_time = 0  # Start time in seconds
    end_time = 15    # End time in seconds

    mp4_to_gif(input_file, output_file, start_time, end_time)
