import paramiko
import pandas as pd
import matplotlib.pyplot as plt
from io import StringIO
import os

# Server details
hostname = 'sylg.fysik.dtu.dk'
username = 'sesasa'
password = os.getenv('Sylg_PASSWORD')

# define the fine name here
file_name='F__00050.csv'

# File paths
file_path1 = '/home/mek/sesasa/Flagellates/3D/torsion_paraSweep/L40_N7/N_study/0.5pi_N0/csv_plots/Ffl_tot__00050.csv'
file_path2 = '/home/mek/sesasa/Flagellates/3D/torsion_paraSweep/L40_N7/N_study/0.5pi_N1/csv_plots/Ffl_tot__00050.csv'
file_path3 = '/home/mek/sesasa/Flagellates/3D/torsion_paraSweep/L40_N7/N_study/0.5pi_N3/csv_plots/Ffl_tot__00050.csv'
file_path4 = '/home/mek/sesasa/Flagellates/3D/torsion_paraSweep/L40_N7/N_study/0.5pi_N5/csv_plots/Ffl_tot__00050.csv'
file_path5 = '/home/mek/sesasa/Flagellates/3D/torsion_paraSweep/L40_N7/N_study/0.5pi_N7/csv_plots/Ffl_tot__00050.csv'
file_path6 = '/home/mek/sesasa/Flagellates/3D/torsion_paraSweep/L40_N7/N_study/0.5pi_N8/csv_plots/Ffl_tot__00050.csv'

# Function to read a CSV file from a remote server
def read_csv_from_server(client, file_path):
    sftp_client = client.open_sftp()
    remote_file = sftp_client.open(file_path)
    file_content = remote_file.read().decode()
    remote_file.close()
    return pd.read_csv(StringIO(file_content))

# Create an SSH client instance
client = paramiko.SSHClient()
client.set_missing_host_key_policy(paramiko.AutoAddPolicy())

try:
    # Connect to the server
    client.connect(hostname, username=username, password=password)

    # Read CSV files
    df1 = read_csv_from_server(client, file_path1)
    df2 = read_csv_from_server(client, file_path2)
    df3 = read_csv_from_server(client, file_path3)
    df4 = read_csv_from_server(client, file_path4)
    df5 = read_csv_from_server(client, file_path5)
    df6 = read_csv_from_server(client, file_path6)

    # Multiply the values of the third column by 1e-12
    df1.iloc[:, 2] *= 1e-12
    df2.iloc[:, 2] *= 1e-12
    df3.iloc[:, 2] *= 1e-12
    df4.iloc[:, 2] *= 1e-12
    df5.iloc[:, 2] *= 1e-12
    df6.iloc[:, 2] *= 1e-12


    # Plotting scaled third column vs first column for both files
    plt.plot(df1.iloc[1:, 0]*50, df1.iloc[1:, 2], label='N0')
    plt.plot(df2.iloc[1:, 0]*50, df2.iloc[1:, 2], label='N1')
    plt.plot(df3.iloc[1:, 0]*50, df3.iloc[1:, 2], label='N3')
    plt.plot(df4.iloc[1:, 0]*50, df4.iloc[1:, 2], label='N5')
    plt.plot(df5.iloc[1:, 0]*50, df5.iloc[1:, 2], label='N7')
    plt.plot(df6.iloc[1:, 0]*50, df6.iloc[1:, 2], label='N8')
    plt.xlabel('t/T')
    plt.ylabel('Fy (pN)')
    plt.title('Force: flagellum + hairs')
    plt.legend()
    plt.show()

except Exception as e:
    print(f"An error occurred: {e}")
finally:
    # Close the connection
    client.close()
