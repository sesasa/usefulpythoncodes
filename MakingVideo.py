import cv2
import os

def create_video(input_folder, output_file, fps):
    image_files = sorted([img for img in os.listdir(input_folder) if img.endswith(".png")])
    
    img_path = os.path.join(input_folder, image_files[0])
    frame = cv2.imread(img_path)
    height, width, layers = frame.shape

    fourcc = cv2.VideoWriter_fourcc(*'DIVX')  # for AVI
    # fourcc = cv2.VideoWriter_fourcc(*'mp4v') # for MP4
    
    out = cv2.VideoWriter(output_file, fourcc, fps, (width, height))
    
    for i in image_files:
        img_path = os.path.join(input_folder, i)
        img = cv2.imread(img_path)
        out.write(img) 
        
        # Optional: show the current frame being written
        cv2.imshow('frame', img)
        cv2.waitKey(1)
        
    out.release()
    cv2.destroyAllWindows()
    print(f"Video {output_file} saved successfully.")

# Parameters
input_folder = "..\\star_err1\\PNGs"
output_file = "..\\star_err1\\PNGs\\output.avi"
fps = 30

# Run the function
create_video(input_folder, output_file, fps)
