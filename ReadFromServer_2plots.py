import paramiko
import pandas as pd
import matplotlib.pyplot as plt
from io import StringIO
import os

# Server details
hostname = 'sylg.fysik.dtu.dk'
username = 'sesasa'
password = os.getenv('Sylg_PASSWORD')

# File paths
file_paths = [
    '/home/mek/sesasa/Flagellates/3D/torsion_paraSweep/L40_N7/N_study/0.5pi_N0/csv_plots/Ffl_tot__00050.csv',
    '/home/mek/sesasa/Flagellates/3D/torsion_paraSweep/L40_N7/N_study/0.5pi_N1/csv_plots/Ffl_tot__00050.csv',
    '/home/mek/sesasa/Flagellates/3D/torsion_paraSweep/L40_N7/N_study/0.5pi_N3/csv_plots/Ffl_tot__00050.csv',
    '/home/mek/sesasa/Flagellates/3D/torsion_paraSweep/L40_N7/N_study/0.5pi_N5/csv_plots/Ffl_tot__00050.csv',
    '/home/mek/sesasa/Flagellates/3D/torsion_paraSweep/L40_N7/N_study/0.5pi_N7/csv_plots/Ffl_tot__00050.csv',
    '/home/mek/sesasa/Flagellates/3D/torsion_paraSweep/L40_N7/N_study/0.5pi_N8/csv_plots/Ffl_tot__00050.csv'
]

# Function to read a CSV file from a remote server
def read_csv_from_server(client, file_path):
    sftp_client = client.open_sftp()
    remote_file = sftp_client.open(file_path)
    file_content = remote_file.read().decode()
    remote_file.close()
    return pd.read_csv(StringIO(file_content))

# Create an SSH client instance
client = paramiko.SSHClient()
client.set_missing_host_key_policy(paramiko.AutoAddPolicy())

try:
    # Connect to the server
    client.connect(hostname, username=username, password=password)
    
    mean_values = []  # List to store mean of the third column for each DataFrame

    # Loop over the file paths to read and process each file
    for i, file_path in enumerate(file_paths, start=1):
        df = read_csv_from_server(client, file_path)
        df.iloc[:, 2] *= 1e-12  # Scale the third column
        
        # Calculate mean of the third column and add it to the list
        mean_values.append(df.iloc[:, 2].mean())
        
        # Plotting scaled third column vs first column for each file
        plt.plot(df.iloc[1:, 0]*50, df.iloc[1:, 2], label=f'case{i-1}')
        
    # Plotting the original data
    plt.xlabel('t/T')
    plt.ylabel('Fy (pN)')
    plt.title('Force: flagellum + hairs')
    plt.legend()
    plt.show()
    
    # Plotting the mean of the scaled third column vs. [1, 2, 3, 4, 5, 6]
    plt.figure()  # Start a new figure for the second plot
    plt.plot([0, 1, 3, 5, 7, 8], mean_values, marker='o', linestyle='-', color='r')
    plt.xlabel('Read Number')
    plt.ylabel('Mean Fy (pN)')
    plt.title('Mean Force vs. Read Number')
    plt.grid(True)
    plt.show()

except Exception as e:
    print(f"An error occurred: {e}")
finally:
    # Close the connection
    client.close()
